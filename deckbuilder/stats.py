import time
import curses
from pathlib import Path
import pandas
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


def draw_deck(decklist):
    time.sleep(1)
    deck = pandas.read_csv(decklist)
    print(deck)


class DeckBuilder:
    def __init__(self, decklist):
        self.decklist = decklist

    def __call__(self, stdscr):
        screen = DeckBuilderScreen(stdscr, self.decklist)
        event_handler = DeckChangeEventHandler(screen)

        observer = Observer()
        decks_dir = str(Path(self.decklist).parent)
        observer.schedule(event_handler, decks_dir)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()


class DeckBuilderScreen:
    def __init__(self, stdscr, decklist):
        """Initialize the screen as a wrapper around a curses window."""
        self.s = stdscr
        self.decklist = decklist
        self.draw_deck()

    def draw_deck(self):
        deck = pandas.read_csv(self.decklist)
        deck.index = range(1, len(deck) + 1)
        self.s.clear()
        self.s.addstr(0, 0, deck.to_string())
        self.s.addstr(
            self.s.getyx()[0] + 2, 0, "Number of cards: {}".format(deck.number.sum())
        )
        self.s.refresh()


class DeckChangeEventHandler(FileSystemEventHandler):
    def __init__(self, screen):
        self.screen = screen

    def on_modified(self, event):
        """How to handle a file modified event."""
        super().on_modified(event)

        if event.src_path == self.screen.decklist:
            time.sleep(1)
            self.screen.draw_deck()
