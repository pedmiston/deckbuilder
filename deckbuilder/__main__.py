import curses
import deckbuilder.stats


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("decklist")
    parser.add_argument(
        "--watch", "-w", help="watch for changes to the deck", action="store_true"
    )
    args = parser.parse_args()
    if args.watch:
        app = deckbuilder.stats.DeckBuilder(args.decklist)
        curses.wrapper(app)
    else:
        deckbuilder.stats.draw_deck(args.decklist)
