#!/usr/bin/env python
from pathlib import Path
import pandas
import pypandoc
import jinja2
import deckbuilder

tmpl = jinja2.Template(
    """```table
---
caption: {{ deck_label }}
header: no
alignment: RL
---
{{ csv }}
```
"""
)


def print_deck(deck_file):
    deck_file = Path(deck_file)
    deck_name = deck_file.stem
    deck_label = " ".join(
        n[0].upper() + n[1:] for n in deck_name.replace("-", " ").split()
    )

    deck, errors = deckbuilder.Deck.from_csv(deck_file)
    if errors:
        for name, error in errors.items():
            print(f"error with card '{name}': {error}")

    deck = deck[["number", "name"]]

    deck_md = f"{deck_name}.md"
    with open(deck_md, "w") as dst:
        dst.write(
            tmpl.render(
                deck_label=deck_label, csv=deck.to_csv(index=False, header=False)
            )
        )

    deck_pdf = f"{deck_name}.pdf"
    pypandoc.convert_file(
        deck_md,
        to="pdf",
        format="md",
        filters=["pantable"],
        extra_args=["--template=style/template.tex"],
        outputfile=deck_pdf,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("deck", help="the path to a csv file containing the deck list")
    args = parser.parse_args()
    print_deck(args.deck)
