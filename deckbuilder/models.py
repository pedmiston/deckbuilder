import collections

import mtgsdk
import pandas

from deckbuilder import exceptions

BASIC_LANDS = ["Plains", "Island", "Swamp", "Mountain", "Forest"]


CardInDeck = collections.namedtuple("CardInDeck", "card number")


class Deck:
    @classmethod
    def from_csv(cls, deck_csv):
        deck_data = pandas.read_csv(deck_csv)
        return cls.from_dataframe(deck_data)

    @classmethod
    def from_dataframe(cls, frame):
        deck = []
        errors = {}
        for info in frame.itertuples():
            try:
                card = Card(name=info.name)
            except exceptions.DecklistException as err:
                errors[info.name] = err
            else:
                card.number_in_deck = info.number
                deck.append(card)
        return cls(deck), errors

    def __init__(self, deck):
        self._deck = pandas.Series(
            deck, index=[card.name for card in deck], name="card"
        )

    def __getitem__(self, key):
        return self._deck[key]

    def __getattr__(self, key):
        return self._deck.apply(lambda card: getattr(card, key))

    def __len__(self):
        return len(self._deck)

    def to_dataframe(self):
        return self.id.reset_index()

    def to_csv(self, deck_csv):
        self.to_dataframe().to_csv(deck_csv, index=False)


class Card:
    def __init__(self, **card_query_kwargs):
        expect_unique = True
        if "name" in card_query_kwargs and card_query_kwargs["name"] in BASIC_LANDS:
            card_query_kwargs["set"] = "M19"
            expect_unique = False
        query_results = mtgsdk.Card.where(**card_query_kwargs).all()
        if len(query_results) == 0:
            raise exceptions.CardNotFoundError()
        elif len(query_results) > 1:
            unique_names = set(card.name for card in query_results)
            if len(unique_names) > 1 and expect_unique:
                raise exceptions.CardNotUniqueError()
        self._card = query_results[0]

    def __getattr__(self, key):
        return getattr(self._card, key)
