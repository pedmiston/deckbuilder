class DecklistException(Exception):
    pass


class CardNotFoundError(DecklistException):
    pass


class CardNotUniqueError(DecklistException):
    pass
