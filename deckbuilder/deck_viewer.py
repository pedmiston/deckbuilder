import pandas
import deckbuilder.card_search


def view_deck(decklist):
    card_names = pandas.read_csv(decklist)["name"].tolist()

    query = dict()

    # try to recognize the sets from the filename
    if "standard" in decklist:
        query["set"] = "|".join(deckbuilder.card_search.standard_sets)

    # try to recognize the rarity from the filename
    if "pauper" in decklist:
        query["rarity"] = "basic land|common"
    elif "peasant" in decklist:
        query["rarity"] = "basic land|common|uncommon"

    return deckbuilder.card_search.view_cards(card_names, **query)
