import pandas

from deckbuilder.models import Deck


def read_deck_csv(deck_csv):
    deck, errors = Deck.from_csv(deck_csv)
    return deck
