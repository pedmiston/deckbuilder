import pathlib
import itertools
import textwrap
from mtgsdk import Card, Set
import pandas
from IPython.display import Image, HTML

# set this otherwise pandas tries to wrap the text (with is really formatted html) with line breaks!
pandas.set_option("display.max_colwidth", -1)


def read_sets_in_format(format):
    sets_in_format_path = pathlib.Path(__file__).parent / "data" / f"sets-in-{format}.csv"
    return pandas.read_csv(sets_in_format_path).set_code.tolist()


standard_sets = read_sets_in_format("standard")
modern_sets = read_sets_in_format("modern")

filler_card_image_url = "https://d1u5p3l4wpay3k.cloudfront.net/mtgsalvation_gamepedia/f/f8/Magic_card_back.jpg?version=2e41a85dcee552394b408b46522059b9"


def cards_to_df(cards):
    df = pandas.DataFrame([c.__dict__ for c in cards])
    if len(df) == 0:
        return pandas.DataFrame()
    df.loc[:, "text"] = df.text.fillna("")
    df["text_html"] = df.text.apply(
        lambda t: "<br>".join(textwrap.wrap(t.replace("\.?\\n", "<br>"), 30))
    )
    df.rename(columns={"name": "card_name"}, inplace=True)
    return df


def path_to_image_html(path):
    return f'<img src="{path}" width="200"/>'


def show_cards(df):
    formatters = {}
    for col in df.columns:
        if col.startswith("image_url"):
            formatters[col] = path_to_image_html
    return HTML(df.to_html(escape=False, formatters=formatters))


def search_mtg_cards(sort_by=None, ordered_card_names=None, **query):

    # Detect basic lands and query them separately.
    # Lands appear in all sets so the query is slow.
    basic_land_results = pandas.DataFrame()
    basic_land_names = "Plains Island Swamp Mountain Forest".split()
    basic_land_in_query = ("name" in query and any(name in basic_land_names for name in query["name"].split("|")))
    if basic_land_in_query:
        names = query["name"].split("|")
        requested_land_names = [name for name in names if name in basic_land_names]
        assert len(requested_land_names) > 0, "error in finding basic lands"

        # Remove the basic lands from the query
        for land in requested_land_names:
            names.remove(land)
        if len(names) > 0:
            query["name"] = "|".join(names)

        # request for basic lands from M19
        basic_land_query = dict(
            name="|".join(requested_land_names),
            set="M19",
        )
        basic_land_results = cards_to_df(Card.where(**basic_land_query).all())

    spells_results = cards_to_df(Card.where(**query).all())
    results = pandas.concat([spells_results, basic_land_results])

    if len(results) == 0:
        print("no cards found with args: {}".format(query))
        return
    results = results.dropna(subset=["image_url"])
    results["color_string"] = results.colors.apply(
        lambda colors: "-".join(colors) if hasattr(colors, "__iter__") else colors
    )
    results = results.drop_duplicates(subset="card_name")

    if ordered_card_names:
        results = results.set_index("card_name").reindex(ordered_card_names)
        results["ix"] = range(len(results))
        results = results.reset_index().set_index("ix")
    else:
        if sort_by is None:
            sort_by = ["color_string", "card_name"]
        results = results.sort_values(by=sort_by)

    # break the results in half and show over multiple columns
    n_columns = 4
    column_assigner = itertools.cycle(range(1, n_columns + 1))
    results["column_ix"] = [next(column_assigner) for row in results.index]

    def assign_rows(chunk):
        chunk["row_ix"] = range(1, len(chunk) + 1)
        return chunk

    results = results.groupby("column_ix").apply(assign_rows)
    results_table = (
        results[["row_ix", "column_ix", "image_url"]]
        .pivot(index="row_ix", columns="column_ix", values="image_url")
        .rename(columns=lambda x: "image_url_{}".format(x))
    )
    return show_cards(results_table.fillna(filler_card_image_url))


def search_standard(**kwargs):
    query = dict(set="|".join(standard_sets))
    query.update(kwargs)
    return search_mtg_cards(**query)


def search_pauper(**kwargs):
    query = dict(set="|".join(modern_sets), rarity="common")
    query.update(kwargs)
    return search_mtg_cards(**query)

def search_pauper_standard(**kwargs):
    query = dict(rarity="common")
    query.update(kwargs)
    return search_standard(**query)

def view_cards(card_names, **kwargs):
    query = dict(name="|".join(card_names))
    query.update(kwargs)
    return search_mtg_cards(sort_by=None, ordered_card_names=card_names, **query)
