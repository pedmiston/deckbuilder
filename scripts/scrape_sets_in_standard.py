#!/usr/bin/env python
import pathlib
import mtgsdk
import requests
import pandas
import bs4

response = requests.get("https://magic.wizards.com/en/content/standard-formats-magic-gathering")

soup = bs4.BeautifulSoup(response.text, "html5lib")
set_names = [s.text.strip() for s in soup.find_all("span", {"class": "nameSet"})]
sets_in_standard = pandas.DataFrame({"set_name": set_names}).drop_duplicates()

def lookup_set_code(set_name):
    try:
        set_code = mtgsdk.Set.where(name=set_name).all()[0].code
    except IndexError as e:
        print("unable to find a code for set name '{}'".format(set_name))
        return ""
    else:
        return set_code

sets_in_standard["set_code"] = sets_in_standard.set_name.apply(lookup_set_code)

dst_dir = pathlib.Path("deckbuilder/data")
if not dst_dir.is_dir():
    dst_dir.mkdir()

sets_in_standard.to_csv(dst_dir / "sets-in-standard.csv", index=False)
