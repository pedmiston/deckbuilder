import pathlib
import mtgsdk
import requests
import pandas
import bs4

response = requests.get("https://magic.wizards.com/en/game-info/gameplay/formats/modern")

soup = bs4.BeautifulSoup(response.text, "html5lib")

set_list_header = soup.find_all(text="The following card sets are permitted in Modern tournaments:")[0]
set_list_element = set_list_header.find_next()
set_names = [e.text.strip() for e in set_list_element.find_all()]
sets_in_modern = pandas.DataFrame({"set_name": set_names}).drop_duplicates()

def lookup_set_code(set_name):
    try:
        set_code = mtgsdk.Set.where(name=set_name).all()[0].code
    except IndexError as e:
        print("unable to find a code for set name '{}'".format(set_name))
        return ""
    else:
        return set_code

sets_in_modern["set_code"] = sets_in_modern.set_name.apply(lookup_set_code)

dst_dir = pathlib.Path("deckbuilder/data")
if not dst_dir.is_dir():
    dst_dir.mkdir()

sets_in_modern.to_csv(dst_dir / "sets-in-modern.csv", index=False)
