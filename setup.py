#!/usr/bin/env python
from distutils.core import setup

setup(
    name="deckbuilder",
    version="1.0",
    description="Magic the Gathering Deck Building Tools",
    author="Pierce Edmiston",
    author_email="pierce.edmiston@gmail.com",
    packages=["deckbuilder"],
    install_requires=[
        "mtgsdk",
        "pandas",
        "jupyter",
        "watchdog",
        "pypandoc",
        "jinja2",
    ],
    package_data={"deckbuilder": ["data/*.csv"]}
)
