import pandas
import pytest

import deckbuilder.models
import deckbuilder.exceptions


def test_read_deck_from_dataframe():
    deck_data = pandas.DataFrame(
        {"name": ["Sparring Construct", "Direct Current"], "number": [2, 2]}
    )
    deck, errors = deckbuilder.models.Deck.from_dataframe(deck_data)
    assert len(errors) == 0
    assert deck.id.tolist() == [
        "e86fa060fbb52e6cd63558998df46632a38a5a48",
        "5632adc09a47e91f731542a06c82070405fb6647",
    ]


def test_read_deck_with_bad_names():
    deck_data = pandas.DataFrame(
        {"name": ["Not A Knight", "Wizard Mommy"], "number": [4, 4]}
    )
    deck, errors = deckbuilder.models.Deck.from_dataframe(deck_data)
    assert len(deck) == 0
    assert len(errors) == 2
    assert isinstance(errors["Not A Knight"], deckbuilder.exceptions.CardNotFoundError)


def test_read_deck_retains_number():
    deck_data = pandas.DataFrame(
        {"name": ["Sparring Construct", "Direct Current"], "number": [2, 2]}
    )
    deck, errors = deckbuilder.models.Deck.from_dataframe(deck_data)
    assert deck.number_in_deck.tolist() == [2, 2]


def test_underspecified_name():
    with pytest.raises(deckbuilder.exceptions.CardNotUniqueError):
        deckbuilder.models.Card(name="Elf")


def test_make_reprinted_card():
    deckbuilder.models.Card(name="Wee Dragonauts")
