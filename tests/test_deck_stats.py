import pytest
import deckbuilder.models


def test_count_basic_lands():
    basic_lands = ["Plains", "Island", "Swamp", "Mountain", "Forest"]
    for name in basic_lands:
        card = deckbuilder.models.Card(name=name)
        cards_in_deck = [deckbuilder.models.CardInDeck(card, 1)]
        deck = deckbuilder.models.Deck(cards_in_deck)
        assert deck.lands == 1
